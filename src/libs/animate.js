export function animate(options) {
    const start = performance.now();

    return requestAnimationFrame(function animate(time) {
        let timeFraction = (time - start) / options.duration;
        if (timeFraction > 1) timeFraction = 1;

        const progress = options.timing ? options.timing(timeFraction) : timeFraction;

        options.draw(progress);

        if (timeFraction < 1) {
            requestAnimationFrame(animate);
        } else {
            options.callback && options.callback();
        }
    });
}