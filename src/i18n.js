import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import ru from 'assets/lang/ru';
import en from 'assets/lang/en';

i18n
    .use(LanguageDetector)
    .init({
        resources: {
            en,
            ru,
        },
        ns: ['translations'],
        defaultNS: 'translations',
        fallbackLng: 'ru',
    });

export default i18n;