import React from 'react';
import ReactDOM, {hydrate} from 'react-dom';
import 'normalize.css/normalize.css';
import { I18nextProvider } from 'react-i18next';

import i18n from './i18n';

import './index.scss';
import App from './features/app';
import registerServiceWorker from './register-service-worker';

hydrate(
    <I18nextProvider i18n={ i18n }>
        <App />
    </I18nextProvider>,
    document.getElementById('root'));
registerServiceWorker();