import React from 'react';
import { translate } from 'react-i18next';
import cn from 'classnames';
import styles from './footer.module.scss';

import anchorIcon from 'assets/images/anchor-icon.svg';
import facebookIcon from './assets/facebook.svg';
import telegramIcon from './assets/telegram.svg';
import phoneIcon from './assets/phone.svg';
import emailIcon from './assets/email.svg';

export const Footer = translate('translations')(({t}) => (
    <div className={styles.footer} name="footer">
        <div className={cn("container", styles.container)}>
            <h2 className={cn("mainHeader", styles.footerTitle)}>Odessa Blockchain HackFest</h2>
            <img src={anchorIcon} alt="Odessa" className='anchorIcon'/>
            <ul className={styles.desc}>
                <li>{t('city')}</li>
                <li>17-18 {t('footer.month')} 2018</li>
            </ul>
            <a href="https://goo.gl/x8TruC" target="_blank" rel="noopener noreferrer" className={styles.buyTickets}>{t('interface.registration')}</a>
            <h3 className={styles.followTitle}>{t('footer.follow')}</h3>
            <ul className={styles.socialsList}>
                <li><a href="/"><img src={facebookIcon} alt="Follow us facebook"/></a></li>
                <li><a href="/"><img src={telegramIcon} alt="Follow us telegram"/></a></li>
            </ul>
            <footer className={styles.footerContainer}>
                <div className={styles.contactItem}>
                    <a href="/"><img src={phoneIcon} alt=""/> 793-891-7938, 952-775-6021</a>
                </div>
                <div className={styles.contactItem}>
                    <a href="/"><img src={emailIcon} alt=""/> contact@example.com</a>
                </div>
            </footer>
        </div>
    </div>
));