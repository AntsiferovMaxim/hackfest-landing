import React from 'react';
import styles from './mentors.module.scss';

import photo from './assets/photo.png';

import {SectionTemplate} from 'ui';

export const Mentors = () => (
    <SectionTemplate
        title="Менторы"
    >
        <div className={styles.bg}/>
        <ul className={styles.mentorList}>
            {mentors.map((item, index) => (
                <li key={index} className={styles.mentorWrapper}>
                    <div className={styles.mentorItem}>
                        <img src={item.photo} alt={item.name} className={styles.mentorPhoto}/>
                        <h4 className={styles.mentorName}>{item.name}</h4>
                        <p className={styles.mentorPosition}>{item.position}</p>
                    </div>
                </li>
            ))}
        </ul>
    </SectionTemplate>
);

const mentors = [
    {photo: photo, name: 'Gregory Sanchez', position: 'Backend Developer'},
    {photo: photo, name: 'Gregory Sanchez', position: 'Backend Developer'},
    {photo: photo, name: 'Gregory Sanchez', position: 'Backend Developer'},
    {photo: photo, name: 'Gregory Sanchez', position: 'Backend Developer'},
    {photo: photo, name: 'Gregory Sanchez', position: 'Backend Developer'},
    {photo: photo, name: 'Gregory Sanchez', position: 'Backend Developer'},
    {photo: photo, name: 'Gregory Sanchez', position: 'Backend Developer'},
    {photo: photo, name: 'Gregory Sanchez', position: 'Backend Developer'},
    {photo: photo, name: 'Gregory Sanchez', position: 'Backend Developer'},
    {photo: photo, name: 'Gregory Sanchez', position: 'Backend Developer'},
];