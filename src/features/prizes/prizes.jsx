import React from 'react';
import { translate } from 'react-i18next';
import styles from './prizes.module.scss';

import madrid from './assets/madrid.jpg';
import seoul from './assets/seoul.jpg';
import korea from './assets/korea.svg';
import spain from './assets/spain.svg';
import {SectionTemplate} from 'ui';

const Prizes = ({t}) => (
    <SectionTemplate
        title={t('prizes.header')}
        description={t('prizes.desc')}
        name='prizes'
    >
        <ul className={styles.prizes}>
            <li className={styles.prize}>
                <img src={korea} alt="" className={styles.prizeCountry}/>
                <img src={seoul} alt="" className={styles.prizeImg}/>
                <p className={styles.prizeDesc}>{t('prizes.korea')}</p>
            </li>
            <li className={styles.prize}>
                <img src={spain} alt="" className={styles.prizeCountry}/>
                <img src={madrid} alt="" className={styles.prizeImg}/>
                <p className={styles.prizeDesc}>{t('prizes.spain')}</p>
            </li>
        </ul>
        <a href="https://goo.gl/x8TruC" target="_blank" rel="noopener noreferrer" className="blue-button">{t('interface.registration')}</a>
    </SectionTemplate>
);

export default translate('translations')(Prizes);