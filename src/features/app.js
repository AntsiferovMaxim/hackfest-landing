import React, {Component} from 'react';
import { translate } from 'react-i18next';

import {Hero} from './hero';
import {StunningResults} from './stunning-results';
import {Schedule} from './schedule';
import {Organizers} from './organizers';
// import {Mentors} from './mentors/mentors';
import {Prizes} from './prizes';
import {Place} from './place';
import {Footer} from './footer/footer';

const AppContext = React.createContext();

class App extends Component {
    state = {
        mobile: App.isMobile(),
        currentLang: 'ru',
        langList: [
            {title: 'Русский', lang: 'ru'},
            {title: 'English', lang: 'en'},
        ]
    };

    componentDidMount() {
        window.addEventListener('resize', this.onResize);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.onResize);
    }

    onResize = () => {
        if (App.isMobile()) {
            this.setState({mobile: true});
        } else {
            this.setState({mobile: false});
        }
    };

    setLang = (lang) => {
        const { i18n } = this.props;

        i18n.changeLanguage(lang);

        this.setState(() => ({currentLang: lang}));
    };

    static isMobile = () => window.innerWidth <= 1200;

    render() {
        return (
            <AppContext.Provider
                value={{
                    ...this.state,
                    setLang: this.setLang
                }}
            >
                <div className='app'>
                    <Hero/>
                    <Organizers/>
                    {/*<Mentors/>*/}
                    <Prizes/>
                    <StunningResults/>
                    <Schedule/>
                    <Place/>
                    <Footer/>
                </div>
            </AppContext.Provider>
        );
    }
}

export {AppContext};

export default translate('translations')(App);
