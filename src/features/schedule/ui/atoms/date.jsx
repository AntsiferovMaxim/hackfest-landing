import React from 'react';
import styles from './date.module.scss';

export const Date = ({day, children}) => (
    <div className={styles.date}>
        <span className={styles.day}>{day}</span>
        <span className={styles.dayOfWeek}>{children}</span>
    </div>
);