import React from 'react';
import styles from './schedule-list.module.scss';
import {Date} from "../atoms";

export const ScheduleList = ({list, day, dayOfWeek, month}) => (
    <ul className={styles.scheduleList}>
        <li className={styles.scheduleItem}>
            <div className={styles.period}/>
            <ul className={styles.periodDescription}>
                <li>
                    <Date day={day}>{dayOfWeek}</Date>
                    <div className={styles.month}>{month}</div>
                </li>
            </ul>
        </li>
        {list.map((item, index) => (
            <li key={dayOfWeek + index} className={styles.scheduleItem}>
                <div className={styles.period}>{item.time}</div>
                <ul className={styles.periodDescription}>
                    {item.desc.map((desc, descIndex) => (
                        <li key={index + 'desc' + descIndex}>{desc}</li>
                    ))}
                </ul>
            </li>
        ))}
    </ul>
);