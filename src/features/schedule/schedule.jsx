import React from 'react';
import {translate} from 'react-i18next';
import styles from './schedule.module.scss';

import {SectionTemplate} from 'ui';
import {ScheduleList} from './ui';

export const Schedule = ({t}) => (
    <SectionTemplate
        title={t('schedule.header')}
        name="schedule"
    >
        <div className={styles.schedule}>
            <div className={styles.month}>{t('schedule.november')}</div>
            <ScheduleList
                list={saturdaySchedule.map(translateSchedule(t))}
                day="17"
                dayOfWeek={t('schedule.saturday')}
                month={t('schedule.november')}
            />
            <ScheduleList
                list={sundaySchedule.map(translateSchedule(t))}
                day="18"
                dayOfWeek={t('schedule.sunday')}
                month={t('schedule.november')}
            />
        </div>
        <a href="https://goo.gl/x8TruC" target="_blank" rel="noopener noreferrer"
           className="blue-button">{t('interface.registration')}</a>
    </SectionTemplate>
);

function translateSchedule(t) {
    return (item) => (
        {
            ...item,
            desc: item.desc.map(desc => t(desc)),
            time: item.translateTime ? t(item.time) : item.time
        })
}

const saturdaySchedule = [
    {name: '1', time: '9:30 - 10:30', desc: ['schedule.saturday_schedule.1']},
    {name: '2', time: '10:00 - 11:20', desc: ['schedule.saturday_schedule.2']},
    {name: '3', time: '11:20 - 12:30', desc: ['schedule.saturday_schedule.3']},
    {name: '4', time: '12:30 - 13:00', desc: ['schedule.saturday_schedule.4']},
    {name: '5', time: '13:00 - 14:30', desc: ['schedule.saturday_schedule.5']},
    {name: '6', time: '14:30 - 15:00', desc: ['schedule.saturday_schedule.6']},
    {name: '7', time: '15:00 - 16:00', desc: ['schedule.saturday_schedule.7']},
    {name: '8', time: '16:00 - 17:30', desc: ['schedule.saturday_schedule.8']},
    {name: '9', time: '16:30 - 18:00', desc: ['schedule.saturday_schedule.9']},
    {name: '10', time: '17:30 - 18:00', desc: ['schedule.saturday_schedule.10']},
    {name: '11', time: '18:00 - 19:30', desc: ['schedule.saturday_schedule.11']},
    {name: '12', time: '20:30 - 21:00', desc: ['schedule.saturday_schedule.12']},
    {
        name: '13',
        translateTime: true,
        time: 'schedule.saturday_schedule.time13',
        desc: ['schedule.saturday_schedule.13']
    },
];

const sundaySchedule = [
    {name: '1', time: '10:00 - 11:00', desc: ['schedule.sunday_schedule.1']},
    {name: '2', time: '11:00 - 11:45', desc: ['schedule.sunday_schedule.2']},
    {name: '3', time: '11:00 - 11:45', desc: ['schedule.sunday_schedule.3']},
    {name: '4', time: '12:00 - 14:00', desc: ['schedule.sunday_schedule.4']},
    {name: '5', time: '14:00 - 14:30', desc: ['schedule.sunday_schedule.5']},
    {name: '6', time: '14:30 - 16:30', desc: ['schedule.sunday_schedule.6']},
    {name: '7', time: '16:30 - 17:00', desc: ['schedule.sunday_schedule.7']},
    {name: '8', time: '16:00 - 17:30', desc: ['schedule.sunday_schedule.8']},
    {name: '9', time: '17:30 - 18:30', desc: ['schedule.sunday_schedule.9']},
    {name: '10', time: '18:30 - 19:00', desc: ['schedule.sunday_schedule.10']},
    {name: '11', time: '19:00 - 21:00', desc: ['schedule.sunday_schedule.11']},
];

export default translate('translations')(Schedule);