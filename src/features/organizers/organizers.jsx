import React, {Component} from 'react';
import cn from 'classnames';
import { translate } from 'react-i18next';
import styles from './organizers.module.scss';

import {SectionTemplate} from 'ui';

import wisbl from './assets/wisbl.svg';
import hive from './assets/hive.svg';

function randomInteger(min, max) {
    let rand = min - 0.5 + Math.random() * (max - min + 1);
    rand = Math.round(rand);
    return rand;
}

const bubblesConfig = [{"size":20,"color":2,"top":83,"left":25},{"size":48,"color":1,"top":29,"left":59},{"size":33,"color":2,"top":77,"left":100},{"size":39,"color":2,"top":93,"left":78},{"size":35,"color":0,"top":50,"left":50},{"size":40,"color":2,"top":81,"left":17},{"size":37,"color":1,"top":25,"left":43},{"size":48,"color":1,"top":87,"left":79},{"size":50,"color":2,"top":21,"left":9},{"size":31,"color":0,"top":4,"left":21},{"size":40,"color":0,"top":84,"left":95},{"size":39,"color":2,"top":54,"left":93},{"size":26,"color":0,"top":79,"left":60},{"size":26,"color":0,"top":89,"left":66},{"size":20,"color":1,"top":88,"left":99}];

class Organizers extends Component {
    constructor() {
        super();

        this.container = React.createRef();
    }

    state = {
        count: 15,
        maxSize: 50,
        minSize: 20,
        colors: [
            'rgba(188, 154, 239, .3)',
            'rgba(151, 37, 163, .3)',
            'rgba(103, 53, 245, .3)',
        ]
    };

    generateBubblesConfig = () => {
        const config = [];

        for (let i = 0; i < this.state.count; i++) {
            const bubble = document.createElement('div');
            const size = randomInteger(this.state.minSize, this.state.maxSize);
            const color = randomInteger(0, this.state.colors.length - 1);
            const top = randomInteger(0, 100);
            const left = randomInteger(0, 100);

            config.push({
                size,
                color,
                top,
                left
            });

            bubble.style.width = size + 'px';
            bubble.style.height = size + 'px';
            bubble.style.top = top + '%';
            bubble.style.left = left + '%';
            bubble.style.background = this.state.colors[color];
            bubble.classList.add(styles.bubble);

            this.container.current.appendChild(bubble);
        }

        console.log(JSON.stringify(config));
    };

    componentDidMount() {
        bubblesConfig.forEach(item => {
            const bubble = document.createElement('div');

            bubble.style.width = item.size + 'px';
            bubble.style.height = item.size + 'px';
            bubble.style.top = item.top + '%';
            bubble.style.left = item.left + '%';
            bubble.style.background = this.state.colors[item.color];
            bubble.classList.add(styles.bubble);

            this.container.current.appendChild(bubble);
        })
    }

    render() {
        const {t} = this.props;

        return (
            <SectionTemplate
                title={t('organizers.header')}
                refd={this.container}
                name='organize'
            >
                <div className={cn(styles.organize, styles.first)}>
                    <div className={styles.organizeImg}>
                        <a href="https://wizbl.io" target="_blank">
                            <img src={wisbl} alt="WIZBL"/>
                        </a>
                    </div>
                    <div className={styles.organizeDesc}>
                        <strong>WIZBL</strong> – {t('organizers.wizbl')}
                    </div>
                </div>
                <div className={cn(styles.organize, styles.last)}>
                    <div className={styles.organizeImg}>
                        <a href="https://hive.id" target="_blank">
                            <img src={hive} alt="Hive"/>
                        </a>
                    </div>
                    <div className={styles.organizeDesc}><strong>Hive</strong> – {t('organizers.hive')}</div>
                </div>
            </SectionTemplate>
        )
    }
}

export default translate('translations')(Organizers);