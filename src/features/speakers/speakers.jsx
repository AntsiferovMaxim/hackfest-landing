import React from 'react';
import styles from './speakers.module.scss';

import {SectionTemplate} from 'ui';

export const Speakers = () => (
    <SectionTemplate
        title="Speakers"
        description={<span>Simultaneous interpretation (English and Russian) is planned for the<br/>Main Stage and the Product Stage</span>}
    >

    </SectionTemplate>
);