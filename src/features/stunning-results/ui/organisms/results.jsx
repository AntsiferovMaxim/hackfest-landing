import React, {Component} from 'react';

import styles from './results.module.scss';
import {ResultItem} from '../molecules';
import img from 'assets/images/pattern-participants.svg';

export class Results extends Component {
    constructor() {
        super();

        this.container = React.createRef();
    }

    componentDidMount() {
        setTimeout(this.draw, 20);
        window.addEventListener('resize', this.onResize);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.onResize);
    }

    onResize = () => {
        this.draw();
    };

    draw = () => {
        const nums = this.container.current.querySelectorAll(`.${styles.num}`);
        const texts = this.container.current.querySelectorAll(`.${styles.text}`);

        Array.from(nums).forEach(num => num.style.fontSize = this.container.current.clientWidth / 10.76 * .9 + 'px');
        Array.from(texts).forEach(num => num.style.fontSize = this.container.current.clientWidth / 28.88 * .9 + 'px');
    };

    render() {
        return (
            <div className={styles.results} ref={this.container}>
                <img src={img} alt=""/>
                {this.props.results.map(item => <ResultItem
                    numClass={styles.num}
                    textClass={styles.text}
                    num={item.count}
                    key={item.title}>{item.title}</ResultItem>
                )}
            </div>
        );
    }
}