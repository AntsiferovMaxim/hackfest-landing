import React from 'react';
import cn from 'classnames';
import styles from './result-item.module.scss';

export const ResultItem = ({num, children, numClass, textClass}) => (
    <div className={styles.wrapper}>
        <div className={cn(styles.num, numClass)}>{num}</div>
        <div className={cn(styles.desc, textClass)}>{children}</div>
    </div>
);