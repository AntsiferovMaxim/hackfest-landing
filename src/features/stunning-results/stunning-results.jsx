import React from 'react';
import cn from 'classnames';
import { translate } from 'react-i18next';
import styles from './stunning-results.module.scss';

import img from 'assets/images/pattern-participants with numbers& text.svg';
import {SectionTemplate} from 'ui';
import {ResultItem, Results} from './ui';

const StunningResults = ({t}) => (
    <SectionTemplate
        title={t('participants.header')}
        name="participants"
    >
        <div className={styles.row}>
            <div className={cn(styles.col, styles.leftCol)}>
                <p className={styles.text}>{t('participants.p_0')}</p>
                <div className={cn(styles.col, styles.stats, styles.leftStats)}>
                    <Results results={results}/>
                </div>
                <p className={styles.text}>{t('participants.p_1')}</p>
                <p className={styles.text}>{t('participants.p_2')}</p>
                <p className={styles.place}>{t('participants.seats_number')}</p>
                <a href="https://goo.gl/x8TruC" target="_blank" rel="noopener noreferrer" className="blue-button">{t('interface.registration')}</a>
            </div>
            <div className={cn(styles.col, styles.stats, styles.rightStats)}>
                <Results results={results}/>
            </div>
        </div>
    </SectionTemplate>
);

const results = [
    {count: 400, title: 'Visitors'},
    {count: 150, title: 'Developers'},
    {count: 20, title: 'Startups'},
    {count: 10, title: 'Speakers'},
    {count: 2, title: 'Days non-stop'},
];

export default translate('translations')(StunningResults);