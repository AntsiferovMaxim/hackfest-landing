import React, {Component} from 'react';
import { translate } from 'react-i18next';
import cn from 'classnames';
import styles from './hero.module.scss';

import {Header} from 'ui';
import anchorIcon from 'assets/images/anchor-icon.svg';

class Hero extends Component {
    render() {
        const {t} = this.props;

        return (
            <div className={styles.heroWrapper}>
                <Header navList={navList.map(translateNav(t))}/>
                <div className={cn('container', styles.heroContainer)}>
                    <h1 className={cn("mainHeader", styles.heroTitle)}>{t('hero.title')}</h1>
                    <img src={anchorIcon} alt="Odessa" className='anchorIcon'/>
                    <ul className={styles.description}>
                        <li className={styles.descriptionItem}>{t('hero.desc.city')}</li>
                        <li className={styles.descriptionItem}>{t('hero.desc.time')}</li>
                    </ul>
                    <a href="https://goo.gl/x8TruC" rel="noopener noreferrer" target="_blank" className="grayButton">Регистрация</a>
                    <div className={styles.card} name='hack'>
                        <p>{t('hero.card.first_p')}</p>
                        <p>{t('hero.card.second_p')}</p>
                    </div>
                </div>
            </div>
        );
    }
}

function translateNav(t) {
    return item => ({
        ...item,
        title: t(item.title)
    })
}

const navList = [
    {title: 'О хакатоне', target: 'hack'},
    {title: 'Организаторы', target: 'organize'},
    {title: 'Призы', target: 'prizes'},
    {title: 'Участники', target: 'participants'},
    {title: 'Расписание', target: 'schedule'},
    {title: 'Контакты', target: 'footer'},
];

export default translate('translations')(Hero);