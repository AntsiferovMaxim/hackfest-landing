import React, {Component} from 'react';
import cn from 'classnames';
import { translate } from 'react-i18next';
import {withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"
import styles from './place.module.scss';

class Place extends Component {
    render() {
        const {t} = this.props;

        return (
            <div className={styles.place}>
                <div className="container">
                    <h3 className="sectionHeader">{t('place.header')}</h3>
                </div>
                <div className={cn('container', styles.container)}>
                    <div className={styles.map}>
                        <PlaceCard t={t}/>
                        <Map
                            loadingElement={<div style={{ height: `100%` }} />}
                            containerElement={<div style={{ height: `751px` }} />}
                            mapElement={<div style={{ height: `100%` }} />}
                            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyA41PT289oXxRO5Adj2AOBfV_IT4EOyfsE&v=3.exp&libraries=geometry"
                        />
                    </div>
                </div>
            </div>
        );
    }
}

const PlaceCard = ({t}) => (
    <div className={styles.popup}>
        <h4>{t('place.terminal')}</h4>
        <p>{t('place.desc')}</p>
        <a href="https://terminal42.com.ua" target='_blank'>https://terminal42.com.ua</a>
        <div className={styles.line}/>
        <ul>
            <li>{t('place.street')}</li>
            <li>{t('place.city')}</li>
        </ul>
    </div>
);

//46.476264, 30.739944

const Map = withScriptjs(withGoogleMap(() => (
    <GoogleMap
        defaultZoom={15}
        defaultCenter={{ lat: 46.476264, lng: 30.739944 }}
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `400px` }} />}
        mapElement={<div style={{ height: `100%` }} />}
        googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyA41PT289oXxRO5Adj2AOBfV_IT4EOyfsE&v=3.exp&libraries=geometry"
    >
        <Marker
            position={{ lat: 46.476264, lng: 30.739944 }}
        />
    </GoogleMap>
)));

export default translate('translations')(Place);