import React, {Component} from 'react';
import cn from 'classnames';
import {Link} from 'react-scroll';
import styles from './mobile-nav.module.scss';
import {animate} from 'libs/animate';
import {AppContext} from 'features/app';

import closeIcon from './assets/burger-close-icon.svg'

export class MobileNav extends Component {
    constructor(props) {
        super(props);

        this.nav = React.createRef();
    }

    state = {
        open: false,
        duration: 200
    };

    handleClick = () => {
        this.setState(({open}) => ({open: !open}));

        if (this.state.open) {
            animate({
                duration: this.state.duration,
                draw: this.hideNav,
                callback: () => this.nav.current.style.display = 'none'
            })
        } else {
            this.nav.current.style.display = 'flex';

            animate({
                duration: this.state.duration,
                draw: this.showNav
            })
        }
    };

    showNav = progress => {
        this.nav.current.style.opacity = progress;
    };

    hideNav = progress => {
        this.nav.current.style.opacity = 1 - progress;
    };

    render = () => (
        <AppContext>
            {({setLang, currentLang, langList}) => (
                <>
                    <button className={styles.mobileButton} onClick={this.handleClick}>
                        <svg width="31px" height="21px" viewBox="0 0 31 21" version="1.1" xmlns="http://www.w3.org/2000/svg">
                            <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                <g transform="translate(-328.000000, -29.000000)" fill="currentColor">
                                    <g transform="translate(-591.000000, -240.000000)">
                                        <g transform="translate(919.000000, 269.000000)">
                                            <rect x="0" y="0" width="31" height="3" rx="1.5"/>
                                            <rect x="0" y="9" width="31" height="3" rx="1.5"/>
                                            <rect x="0" y="18" width="31" height="3" rx="1.5"/>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </button>
                    <nav ref={this.nav} className={styles.mobileNavList}>
                        <button className={cn(styles.mobileButton, styles.mobileButtonClose)} onClick={this.handleClick}>
                            <img src={closeIcon} alt=""/>
                        </button>
                        <ul className={styles.list}>
                            {this.props.navList.map((item, index) => (
                                <li key={index}><Link to={item.target} smooth={true} spy={true} onClick={this.handleClick}>{item.title}</Link></li>
                            ))}
                        </ul>
                        <a href="https://goo.gl/x8TruC" target="_blank" rel="noopener noreferrer" className="blue-button">Регистрация</a>
                        <div className={styles.lang}>
                            <ul className={styles.langList}>
                                {langList.map(item => (
                                    <li
                                        key={item.lang}
                                        onClick={() => setLang(item.lang) || this.handleClick()}
                                        className={cn({[styles.langActive]: currentLang === item.lang})}
                                    >{item.title}</li>
                                ))}
                            </ul>
                        </div>
                    </nav>
                </>
            )}
        </AppContext>
    );
}