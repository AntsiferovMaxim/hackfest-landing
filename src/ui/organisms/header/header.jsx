import React, {Component} from 'react';
import cn from 'classnames';
import styles from './header.module.scss';
import {AppContext} from 'features/app';

import logo from 'assets/images/hackfest-logo.svg';
import colorLogo from 'assets/images/logo-color.svg';
import {DesktopNav} from './desktop-nav';
import {MobileNav} from './mobile-nav';

export class Header extends Component {
    state = {
        change: false,
        current: 'hack'
    };

    componentDidMount() {
        window.addEventListener('scroll', this.onScroll);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.onScroll);
    }

    onScroll = () => {
        const offset = window.pageYOffset;
        if (offset < this.getOffset()) {
            this.setState({change: false});
        } else {
            this.setState({change: true});
        }
    };

    getOffset() {
        return this.isMobile() ? 20 : 100;
    }

    isMobile() {
        return window.innerWidth <= 1200;
    }

    onClick = (current) => {
        this.setState(() => ({current}))
    };
    
    render() {
        return (
            <AppContext.Consumer>
                {({mobile}) => (
                    <header className={cn(styles.header, {[styles.change]: this.state.change})}>
                        <div className={cn('container', styles.headerContainer)}>
                            <a href="/" className={styles.logo}><img src={this.state.change ? colorLogo : logo} alt="HackFest Odessa"/></a>
                            {mobile ?
                                <MobileNav navList={this.props.navList}/> :
                                <DesktopNav change={this.state.change} activeClass={styles.navActive} navList={this.props.navList} onClick={this.onClick} current={this.state.current}/>}
                        </div>
                    </header>
                )}
            </AppContext.Consumer>
        );
    }
}