import React from 'react';
import styles from "./desktop-nav.module.scss";
import cn from "classnames";
import {LangSelect} from "./lang-select";
import {Link} from 'react-scroll';

export const DesktopNav = ({navList, activeClass, change, onClick, current}) => (
    <>
        <div className={styles.dnav}>
            <nav className={styles.nav}>
                <ul className={styles.navList}>
                    {navList.map((item, index) => (
                        <li key={index}>
                            <Link onSetActive={onClick} className={cn(styles.navLink, {[activeClass]: item.target === current})} to={item.target} smooth={true} spy={true} onClick={() => onClick(item.target)}>{item.title}</Link>
                        </li>
                    ))}
                </ul>
            </nav>
            <a href="https://goo.gl/x8TruC" target="_blank" rel="noopener noreferrer" className={cn({"ticketButton": !change}, {[styles.headerButton]: change}, styles.button)}>Регистрация</a>
            <LangSelect/>
        </div>
    </>
);