import React, {Component} from 'react';
import cn from 'classnames';
import styles from './section-template.module.scss';

export class SectionTemplate extends Component {
    render() {
        const {title, description, children, refd, name} = this.props;

        return (
            <section ref={refd} className={styles.sectionWrapper} name={name}>
                <div className={cn('container', styles.sectionContainer)}>
                    <h3 className="sectionHeader"> {title}</h3>
                    {description && <p className={styles.sectionDescription}>{description}</p>}
                    {children}
                </div>
            </section>
        )
    }
};